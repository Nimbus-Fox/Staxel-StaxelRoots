﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.FoxCore.Classes;
using Plukit.Base;
using Staxel.TileStates.Docks;

namespace NimbusFox.StaxelRoots.Classes.DatabaseRecords {
    public class DockRecord : RootRecord {
        public DockRecord(BlobDatabase database, Blob blob) : base(database, blob) { }

        public string DockName {
            get => _blob.GetString(nameof(DockName), "");
            set {
                _blob.SetString(nameof(DockName), value);
                Save();
            }
        }

        public string ItemCodeBefore {
            get => _blob.GetString(nameof(ItemCodeBefore), "");
            set {
                _blob.SetString(nameof(ItemCodeBefore), value);
                Save();
            }
        }

        public int ItemCountBefore {
            get => (int) _blob.GetLong(nameof(ItemCountBefore), 0);
            set {
                _blob.SetLong(nameof(ItemCountBefore), value);
                Save();
            }
        }

        public string ItemCodeAfter {
            get => _blob.GetString(nameof(ItemCodeAfter), "");
            set {
                _blob.SetString(nameof(ItemCodeAfter), value);
                Save();
            }
        }

        public int ItemCountAfter {
            get => (int) _blob.GetLong(nameof(ItemCountAfter), 0);
            set {
                _blob.SetLong(nameof(ItemCountAfter), value);
                Save();
            }
        }

        public bool PlayerDock {
            get => _blob.GetBool(nameof(PlayerDock), false);
            set {
                _blob.SetBool(nameof(PlayerDock), value);
                Save();
            }
        }
    }
}
