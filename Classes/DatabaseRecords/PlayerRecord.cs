﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.FoxCore.Classes;
using Plukit.Base;

namespace NimbusFox.StaxelRoots.Classes.DatabaseRecords {
    public class PlayerRecord : BaseRecord {
        public PlayerRecord(BlobDatabase database, Blob blob) : base(database, blob) { }

        public string UID {
            get => _blob.GetString(nameof(UID), "");
            set {
                _blob.SetString(nameof(UID), value);
                Save();
            }
        }

        public string PlayerName {
            get => _blob.GetString(nameof(PlayerName), "");
            set {
                _blob.SetString(nameof(PlayerName), value);
                Save();
            }
        }
    }
}
