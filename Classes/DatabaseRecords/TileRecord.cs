﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.FoxCore.Classes;
using Plukit.Base;

namespace NimbusFox.StaxelRoots.Classes.DatabaseRecords {
    public class TileRecord : RootRecord {
        public TileRecord(BlobDatabase database, Blob blob) : base(database, blob) { }

        public string TileCode {
            get => _blob.GetString(nameof(TileCode), "");
            set {
                _blob.SetString(nameof(TileCode), value);
                Save();
            }
        }

        public uint Variant {
            get => (uint) _blob.GetLong(nameof(Variant), 0);
            set {
                _blob.SetLong(nameof(Variant), value);
                Save();
            }
        }

        public uint Alt {
            get => (uint) _blob.GetLong(nameof(Alt), 0);
            set {
                _blob.SetLong(nameof(Alt), value);
                Save();
            }
        }
    }
}
