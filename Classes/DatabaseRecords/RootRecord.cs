﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.FoxCore.Classes;
using Plukit.Base;

namespace NimbusFox.StaxelRoots.Classes.DatabaseRecords {
    public class RootRecord : BaseRecord {
        public RootRecord(BlobDatabase database, Blob blob) : base(database, blob) { }

        public Vector3D Location {
            get => _blob.Contains(nameof(Location)) ? _blob.FetchBlob(nameof(Location)).GetVector3D() : Vector3D.Zero;
            set {
                _blob.FetchBlob(nameof(Location)).SetVector3D(value);
                Save();
            }
        }

        public string Action {
            get => _blob.GetString(nameof(Action), "");
            set {
                _blob.SetString(nameof(Action), value);
                Save();
            }
        }

        public string Player {
            get => _blob.GetString(nameof(Player), "");
            set {
                _blob.SetString(nameof(Player), value);
                Save();
            }
        }

        public DateTime Time {
            get => new DateTime(_blob.GetLong(nameof(Time), 0));
            set {
                _blob.SetLong(nameof(Time), value.Ticks);
                Save();
            }
        }
    }
}
