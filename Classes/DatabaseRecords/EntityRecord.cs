﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.FoxCore.Classes;
using Plukit.Base;
using Staxel.Logic;

namespace NimbusFox.StaxelRoots.Classes.DatabaseRecords {
    public class EntityRecord : RootRecord {
        public EntityRecord(BlobDatabase database, Blob blob) : base(database, blob) { }

        public EntityId EntityId {
            get => _blob.GetLong(nameof(EntityId), 0);
            set {
                _blob.SetLong(nameof(EntityId), value.Id);
                Save();
            }
        }

        public string ItemCode {
            get => _blob.GetString(nameof(ItemCode), "");
            set {
                _blob.SetString(nameof(ItemCode), value);
                Save();
            }
        }
    }
}
