﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.FoxCore;
using NimbusFox.FoxCore.Classes;
using NimbusFox.StaxelRoots.Classes.DatabaseRecords;
using NimbusFox.StaxelRoots.Patches;
using Plukit.Base;
using Staxel;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Player;
using Staxel.Tiles;
using Staxel.Translation;

namespace NimbusFox.StaxelRoots {
    public class RootsHook : IFoxModHookV3 {
        internal static BlobDatabase RootsDatabase { get; private set; }
        internal static BlobDatabase PlayerDatabase { get; private set; }

        internal static Fox_Core FoxCore { get; private set; }

        private bool _selfCheck = false;

        public RootsHook() {
            FoxCore = new Fox_Core("NimbusFox", "StaxelRoots", "0.1");

            RootsDatabase = new BlobDatabase(FoxCore.SaveDirectory.ObtainFileStream("roots.db", FileMode.OpenOrCreate, FileAccess.Read), FoxCore.LogError);
            PlayerDatabase = new BlobDatabase(FoxCore.ConfigDirectory.ObtainFileStream("player.db", FileMode.OpenOrCreate, FileAccess.Read), FoxCore.LogError);

            DockSitePatches.InitPatches();
            EntityLogicPatches.InitPatches();
        }

        public void Dispose() {
            RootsDatabase?.Dispose();
        }
        public void GameContextInitializeInit() { }
        public void GameContextInitializeBefore() { }
        public void GameContextInitializeAfter() { }
        public void GameContextDeinitialize() { }
        public void GameContextReloadBefore() { }
        public void GameContextReloadAfter() { }
        public void UniverseUpdateBefore(Universe universe, Timestep step) { }

        public void UniverseUpdateAfter() {
            if (FoxCore.WorldManager.Universe.Server) {
                RootsDatabase.Save();
            }
        }
        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            if (entity?.PlayerEntityLogic == null) {
                return true;
            }

            if (!_selfCheck) {
                _selfCheck = true;
                var check = GameContext.ModdingController.CanPlaceTile(entity, location, tile, accessFlags);
                if (check) {
                    var record = RootsDatabase.CreateRecord<TileRecord>(Guid.NewGuid());

                    record.TileCode = tile.Configuration.Code;
                    record.Alt = tile.Alt();
                    record.Variant = tile.Variant();
                    record.Location = location.ToVector3D();
                    record.Action = "nimbusfox.staxelroots.verb.placed";
                    record.Player = entity.PlayerEntityLogic.Uid();
                    record.Time = DateTime.Now;
                }
                _selfCheck = false;
            }

            return true;
        }

        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            if (entity?.PlayerEntityLogic == null) {
                return true;
            }

            if (!FoxCore.WorldManager.Universe.ReadTile(location, accessFlags, out var tile)) {
                return true;
            }

            if (!_selfCheck) {
                _selfCheck = true;
                var check = GameContext.ModdingController.CanRemoveTile(entity, location, accessFlags);
                if (check) {
                    var record = RootsDatabase.CreateRecord<TileRecord>(Guid.NewGuid());

                    record.TileCode = tile.Configuration.Code;
                    record.Alt = tile.Alt();
                    record.Variant = tile.Variant();
                    record.Location = location.ToVector3D();
                    record.Action = "nimbusfox.staxelroots.verb.removed";
                    record.Player = entity.PlayerEntityLogic.Uid();
                    record.Time = DateTime.Now;
                }
                _selfCheck = false;
            }

            return true;
        }

        public void ClientContextInitializeInit() { }
        public void ClientContextInitializeBefore() { }
        public void ClientContextInitializeAfter() { }
        public void ClientContextDeinitialize() { }
        public void ClientContextReloadBefore() { }
        public void ClientContextReloadAfter() { }
        public void CleanupOldSession() { }
        public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
            if (entity?.PlayerEntityLogic == null) {
                return true;
            }

            if (!_selfCheck) {
                _selfCheck = true;
                var check = GameContext.ModdingController.CanInteractWithTile(entity, location, tile);
                if (check) {
                    
                }
                _selfCheck = false;
            }

            return true;
        }

        public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
            return true;
        }

        public void OnPlayerLoadAfter(Blob blob) { }
        public void OnPlayerSaveBefore(PlayerEntityLogic logic, out Blob saveBlob) {
            saveBlob = null;
        }

        public void OnPlayerSaveAfter(PlayerEntityLogic logic, out Blob saveBlob) {
            saveBlob = null;
        }

        public void OnPlayerConnect(Entity entity) {
            var record = PlayerDatabase.SearchRecords<PlayerRecord>(x => x.UID == entity.PlayerEntityLogic.Uid())
                .FirstOrDefault();

            if (record == default(PlayerRecord)) {
                record = PlayerDatabase.CreateRecord<PlayerRecord>(Guid.NewGuid());

                record.PlayerName = entity.PlayerEntityLogic.DisplayName();
                record.UID = entity.PlayerEntityLogic.Uid();
            } else {
                record.PlayerName = entity.PlayerEntityLogic.DisplayName();
            }
        }

        public void OnPlayerDisconnect(Entity entity) { }
    }
}
