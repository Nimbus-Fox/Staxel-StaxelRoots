﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.StaxelRoots.Classes.DatabaseRecords;
using Staxel.Logic;

namespace NimbusFox.StaxelRoots.Patches {
    internal static class EntityLogicPatches {
        internal static void InitPatches() {
            RootsHook.FoxCore.PatchController.Add(typeof(EntityLogic), "Interact", null, null,
                typeof(EntityLogicPatches), nameof(InteractAfter));
        }

        private static void InteractAfter(EntityLogic __instance, Entity entity, EntityUniverseFacade facade,
            ControlState main, ControlState alt) {
            if (!alt.DownClick && !main.DownClick) {
                return;
            }

            if (entity?.PlayerEntityLogic == null) {
                return;
            }

            if (!entity.PlayerEntityLogic.LookingAtEntity(out var target)) {
                return;
            }

            if (!facade.TryGetEntity(target, out var targetEntity)) {
                return;
            }

            var record = RootsHook.RootsDatabase.CreateRecord<EntityRecord>(Guid.NewGuid());

            record.EntityId = targetEntity.Id;

            if (!entity.Inventory.ActiveItem().IsNull()) {
                record.ItemCode = entity.Inventory.ActiveItem().Item.Configuration.Code;

                if (record.ItemCode == "NimbusFox.StaxelRoots.item.investigationTool") {
                    record.ItemCode = "";
                }
            }

            record.Location = targetEntity.Physics.Position;
            record.Player = entity.PlayerEntityLogic.Uid();
            record.Time = DateTime.Now;
        }
    }
}
