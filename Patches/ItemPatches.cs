﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.StaxelRoots.Classes.DatabaseRecords;
using Staxel.Items;
using Staxel.Logic;

namespace NimbusFox.StaxelRoots.Patches {
    internal static class ItemPatches {
        internal static void InitPatches() {

        }

        private static void ControlBefore(CraftItem __instance, Entity entity, EntityUniverseFacade facade,
            ref ControlState main, ref ControlState alt) {
            if (!main.DownClick && !alt.DownClick) {
                return;
            }

            if (entity?.PlayerEntityLogic == null) {
                return;
            }

            if (!entity.PlayerEntityLogic.IsAdmin()) {
                return;
            }

            if (entity.Inventory.ActiveItem().IsNull()) {
                return;
            }

            var item = entity.Inventory.ActiveItem().Item;

            if (item.Configuration.Code != "nimbusfox.staxelroots.item.investigationTool") {
                return;
            }

            entity.PlayerEntityLogic.LookingAtTile(out var target, out var adjacent);

            if (main.DownClick) {
                var entries =
                    RootsHook.RootsDatabase.SearchRecords<TileRecord>(x => x.Location == adjacent.ToVector3D());

                
            } else if (alt.DownClick) {

            }

            alt = new ControlState();
            main = new ControlState();
        }
    }
}
