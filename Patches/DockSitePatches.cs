﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.StaxelRoots.Classes.DatabaseRecords;
using Staxel.Logic;
using Staxel.Player;
using Staxel.TileStates.Docks;

namespace NimbusFox.StaxelRoots.Patches {
    internal static class DockSitePatches {

        internal static void InitPatches() {
            RootsHook.FoxCore.PatchController.Add(typeof(DockSite), "TryDock", typeof(DockSitePatches),
                nameof(TryDockBefore), typeof(DockSitePatches), nameof(TryDockAfter));

            RootsHook.FoxCore.PatchController.Add(typeof(DockSite), "TryUnDock", typeof(DockSitePatches),
                nameof(TryUnDockBefore), typeof(DockSitePatches), nameof(TryUnDockAfter));

            RootsHook.FoxCore.PatchController.Add(typeof(DockSite), "DockPlayer", typeof(DockSitePatches),
                nameof(DockPlayerBefore));

            RootsHook.FoxCore.PatchController.Add(typeof(DockSite), "UnDockPlayer", typeof(DockSitePatches),
                nameof(UnDockPlayerBefore));
        }

        private static void TryDockBefore(DockRecord ___record, DockSite __instance, Entity entity, EntityUniverseFacade facade,
            ItemStack stack, uint rotation) {

            if (entity?.PlayerEntityLogic == null) {
                return;
            }

            ___record = RootsHook.RootsDatabase.CreateRecord<DockRecord>(Guid.NewGuid());

            ___record.DockName = __instance.Config.SiteName;
            ___record.Location = __instance.GetEntity().Physics.Position;
            ___record.Player = entity.PlayerEntityLogic.Uid();
            ___record.Time = DateTime.Now;
            ___record.Action = __instance.DockVerb();

            if (!__instance.IsEmpty()) {
                if (!__instance.DockedItem.Stack.IsNull()) {
                    ___record.ItemCodeBefore = __instance.DockedItem.Stack.Item.Configuration.Code;
                    ___record.ItemCountBefore = __instance.DockedItem.Stack.Count;
                }
            }
        }

        private static void TryDockAfter(DockRecord ___record, DockSite __instance, Entity entity,
            EntityUniverseFacade facade, ItemStack stack, uint rotation) {

            if (entity?.PlayerEntityLogic == null) {
                return;
            }

            if (!__instance.IsEmpty()) {
                if (!__instance.DockedItem.Stack.IsNull()) {
                    ___record.ItemCodeAfter = __instance.DockedItem.Stack.Item.Configuration.Code;
                    ___record.ItemCountAfter = __instance.DockedItem.Stack.Count;
                }
            }
        }

        private static void TryUnDockBefore(DockRecord ___record, DockSite __instance, PlayerEntityLogic player,
            EntityUniverseFacade facade, int quantity) {
            ___record = RootsHook.RootsDatabase.CreateRecord<DockRecord>(Guid.NewGuid());

            ___record.DockName = __instance.Config.SiteName;
            ___record.Location = __instance.GetEntity().Physics.Position;
            ___record.Player = player.Uid();
            ___record.Time = DateTime.Now;
            ___record.Action = __instance.UndockVerb();

            if (!__instance.IsEmpty()) {
                if (!__instance.DockedItem.Stack.IsNull()) {
                    ___record.ItemCodeBefore = __instance.DockedItem.Stack.Item.Configuration.Code;
                    ___record.ItemCountBefore = __instance.DockedItem.Stack.Count;
                }
            }
        }

        private static void TryUnDockAfter(DockRecord ___record, DockSite __instance, PlayerEntityLogic player,
            EntityUniverseFacade facade, int quantity) {
            if (!__instance.IsEmpty()) {
                if (!__instance.DockedItem.Stack.IsNull()) {
                    ___record.ItemCodeAfter = __instance.DockedItem.Stack.Item.Configuration.Code;
                    ___record.ItemCountAfter = __instance.DockedItem.Stack.Count;
                }
            }
        }

        private static void DockPlayerBefore(DockRecord ___record, DockSite __instance, Entity player) {
            if (player?.PlayerEntityLogic == null) {
                return;
            }

            ___record = RootsHook.RootsDatabase.CreateRecord<DockRecord>(Guid.NewGuid());
            ___record.DockName = __instance.Config.SiteName;
            ___record.Location = __instance.GetEntity().Physics.Position;
            ___record.Player = player.PlayerEntityLogic.Uid();
            ___record.Time = DateTime.Now;
            ___record.Action = __instance.DockVerb();
            ___record.PlayerDock = true;
        }

        private static void UnDockPlayerBefore(DockRecord ___record, DockSite __instance, bool supressEffect) {
            var entity = RootsHook.FoxCore.UserManager.GetPlayerEntities()
                .FirstOrDefault(x => x.Id == __instance.DockedPlayer);

            if (entity == default(Entity)) {
                return;
            }
            
            ___record = RootsHook.RootsDatabase.CreateRecord<DockRecord>(Guid.NewGuid());
            ___record.DockName = __instance.Config.SiteName;
            ___record.Location = __instance.GetEntity().Physics.Position;
            ___record.Player = entity.PlayerEntityLogic.Uid();
            ___record.Time = DateTime.Now;
            ___record.Action = __instance.UndockVerb();
            ___record.PlayerDock = true;
        }
    }
}
